## NAILING AUTOMATED PROCESSES
*Containerization & Pipelines: Tester's ultimate weapon*

### *Virtualization vs Containerization*

Introduction to this topics as a way to have your own isolated server to mount a test environment.
#### Subtopics:
* Virtualization
  - What is?
  - Features.
  - Types.
  - Hypervisor.
  - Advantages of virtual machines.
  - Tools.
* Containerization
  - What is a container?
  - How containers work?
  - Tools.
* Virtual machines vs containers
  - About their diagrams.
  - About their use cases.
  - About their existence in cloud providers.

### *Vagrant + Docker*

Deepening of these virtualization and containerization tools, respectively. 

#### Subtopics:
* Why Vagrant?
    - Vagrant's workflow.
    - Typical Vagrantfile set up.
* Why Docker?
  - Docker's workflow.
  - A common Dockerfile.
  - Docker images for testing
* Why both?
  - Vagrant + Docker
* Interactive vagrant + docker demo
  - Clone this repository
  - Run the next command in your console:
    `cd cicd/vagrant-plus-docker/`
  - Follow the *"How to use this folder"* according to [README.md](https://gitlab.com/cd-plus-ta-workshop/cicd/-/tree/master/vagrant-plus-docker)  instructions of this directory.
* Useful vocabulary & commands 

### *DevOps Cycle and Pipeline* 

DevOps life cycle as a guide to join CI/CD with test automation, and how organize this in a pipeline.

#### Subtopics:
* A fun quiz.
* Devops life cycle
  - Continuous development.
  - Continuous testing.
  - Continuous deployment.
  - Continuous monitoring.  
* Continuous testing
  - Advantages.
  - Shifting Left.
* Pipeline
  - What is?
  - Process inside.
  - Characteristics.
  - Types.
* Jenkins pipeline
  - Stages.
  - Best practices.
  - Useful vocabulary.

### *Jenkins as CI/CD tool*

Deepening in Jenkins tool with a complete provisioning of concepts, interaction, and an isolated environment with a focus on testing.

This presentation has an interactive process with participants constantly. In order to follow the expositor you must to:

- Clone this repository.
- Run the next command in your console: 
  `cd cicd/jenkins/`
- Follow the [README.md](https://gitlab.com/cd-plus-ta-workshop/cicd/-/tree/master/jenkins) of this directory. 

**Note: Please use Git Bash*

#### Subtopics:
* Installation
  - Using configuration management.
  - Jenkins with ansible.
  - A brief view of Jenkins dashboard.
* Credentials
  - Types.
  - Credentials creation.
* Job types
  - Advantages and disadvantages of each one.
  - Job creation.
* Plugins
  - What are?.
  - Useful plugins for testing.
  - Running jobs and report results.
* Running jobs in different nodes
  - Agents and executors.
  - Definition in the pipelines.
  - Master-slave architecture.
  - Launch methods.
  - Take a look at the nodes configuration in Jenkins dashboard.
* Additional helpful tools.