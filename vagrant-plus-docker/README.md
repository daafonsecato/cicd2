### *Vagrant + Docker*

Deepening of these virtualization and containerization tools, respectively. 

#### Subtopics:
* Why Vagrant?
    - Vagrant's workflow.
    - Typical Vagrantfile set up.
* Why Docker?
  - Docker's workflow.
  - A common Dockerfile.
  - Docker images for testing
* Why both?
  - Vagrant + Docker
* Interactive vagrant + docker demo
* Useful vocabulary & commands 
- - -

In this folder, you'll find an application called **emoji-search** developed in react, also a docker-compose.yml file which basically contains the information for recreating micro-services app based on containers from a remote repository on Github. As well as a Vagrantfile which sets all the needed configuration for creating a virtual machine included installing docker and docker-compose, as well as setting the networking part of the virtual machine and some other specs.

## How to use this folder

### Prerequisites

* Install VirtualBox
  * Here is a [link](https://www.virtualbox.org/wiki/Downloads) for you to download it.
    In case the VirtualBox pages are blocked for you, follow this [link](https://gitlab.com/cd-plus-ta-workshop/cicd/-/tree/master/virtual-box-installers) and download your respective installer. 
  * How to in [Windows](https://youtu.be/v520HJNiETE?t=55), [Linux](https://www.youtube.com/watch?v=OD_mA2riAhU) and [macOS](https://youtu.be/bQVAT5SntKk?t=36) in case you need it. 
    <br/>
     >In the special case that you have a MacOS with Catalina version is possible the installation failed so please go to  `System Preferences - Security & Privacy - Allow apps downloaded from` section. 
     Here you will see the package that was blocked, allow the package and try to install again VirtualBox. 

    ![packageunblocked](../../virtual-box-installers/UnblockPackageCatalinaMacOSversion.jpG)
    <br/>
* Install Vagrant and operative system box.
  * How to in [Windows](https://www.youtube.com/watch?v=d6MBW9MoIDk), [Ubuntu](https://www.youtube.com/watch?v=bQTBBV-3T6k) and [macOS](https://youtu.be/OzJlR2qcTHU?t=37) in case you need it.
    In case you have a Windows as OS, you have to go to `Turn Windows feature on or off` on your pc and disable `Hyper-V` function, before install vagrant. You have to restart your pc to apply this change.
  * Here is the [link](https://www.vagrantup.com/downloads.html) for you to download it.*
  * Open a terminal.
  * Verify if the installation was successfully running the next command:
  `vagrant --version`
  * We have to download a Vagrant box to have the version that we want for our OS, so run on the terminal the next command: 
  `vagrant box add ubuntu/bionic64`

**Note: Please install the latest version (2.2.9)*

Now that our computer has installed and configured Vagrant and VirtualBox, download this repo with a .zip or clone it into your computer.

### Steps to start the docker local environment:

* Make sure that you are on the directory on which you can find the Vagrantfile.
* Open a terminal on that directory. Please use *Git Bash*.
* We have to download a Vagrant box to have the version that we want for our OS, so run on the terminal the next command: 
`vagrant box add ubuntu/bionic64`
* Now you are ready to up the machine:
`vagrant up`
This takes time please leave run and wait until ones finished to follow the next steps.
* Verify if the machine was created. Go to `Oracle VM VirtualBox` program, the machine should appear there with "docker-local-environment".
* After the installation and configuration are all set up, if you want to connect from your computer to your local machine run:
`vagrant ssh`

Now you have your docker local environment up and ready to go!

## Vagrant + Docker demo

Once you have your environment up and ready, and using ssh to access your vagrant VM, you should follow the next steps in order to have a dockerized app.

### (Optional) Run the app without docker
The idea of these steps is to have a clear view about what is needed to run the **emoji-search** application
1.  Move into the **emoji-search** directory using `cd emoji-search`.
2.  Run `npm install` and `npm start` and wait until these commands finish.
3.  If everything went well, you should be able to open the emoji app on your browser at this URL: [emoji app](http://localhost:3000/emoji-search).

### Dockerize the app
The idea of these steps is to have a clear view about what is needed to dockerize and run inside a container the **emoji-search** application.
1.  Move into the **emoji-search** directory using `cd emoji-search`.
2.  Create a Dockerfile, you can do it by executing `touch Dockerfile` or using an editor eg. vim `vim Dockerfile`.
3.  We need to include the required lines in that Dockerfile, so include on that file the next:

    >FROM node:12-slim
    >
    >WORKDIR /app
    >
    >COPY . /app
    >
    >RUN npm install
    >
    >EXPOSE 3000
    >
    >CMD ["npm", "start"]
    
    Can you tell what every line means?
    
4.  After save the Dockerfile with the above lines you are ready to build your first docker image, to make that happen run `docker build -t emoji-search:v1 .`.
5.  When docker finalize to build the image you can run the dockerized emoji app with `docker run -p 3000:3000 -it emoji-search:v1`.
6.  If everything went well you are able to use the emoji app on your PC accessing in any browser at `http://localhost:3000/emoji-search`.
