pipeline {
    agent any
    environment {
		GIT_CREDENTIALS = credentials ('git-credentials')
        DOCKER_CREDENTIALS = 'docker-credentials'
        registry = "daafonsecato/challengecicd"
        dockerImage = ''
	}

	stages {
		stage ('PREPARE') {
            steps {
                cleanWs()
                echo "** Cloning git repository **"
                git credentialsId: 'git-credentials', url: 'https://github.com/daafonsecato/cichallenge.git'
                sh 'npm install' // se mantiene para el HTML-Test-Report
            }
        }
        stage ('BUILD') {
            steps {
                echo "** Building docker image **"
                script {
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
				}
                //sh "docker build -t daafonsecato/challengecicd:$BUILD_NUMBER --pull=true ."
                //sh "docker build -t challengecicd:$BUILD_NUMBER ."
            }
        }
        stage ('TEST') {
            steps {
                echo "** Making tests. **"
                sh 'npm test' // se mantiene para el HTML-Test-Report
            }
        }
        stage ('REPORTS'){
			steps{
                publishHTML([
					allowMissing         : true,
					alwaysLinkToLastBuild: true,
					keepAll              : true,
					reportDir            : "",
					reportFiles          : 'test-report.html',
					reportName           : 'HTML Reports',
					reportTitles         : 'Standard Report'
				])
			}
		}
        stage('PUSH') {
            steps {
                echo "** Pushing the image. **"
                echo "** registryCredential **"
                echo "** $BUILD_NUMBER **"
                echo "** latest **"
                script {
                    docker.withRegistry( 'https://registry.hub.docker.com', DOCKER_CREDENTIALS ) {
                        dockerImage.push("$BUILD_NUMBER")
                        dockerImage.push('latest')
                    }
                }
            }
        }
        stage ('DEPLOY'){
			steps{
                echo "** Deploying the docker container. **"
                // sh "if ! docker ps --format '{{.Names}}' | grep -w c1 &> /dev/null; then docker rm --force /c1 && docker run -d --name c1 -p 8000:8000 challengecicd:$BUILD_NUMBER; else docker run -d --name c1 -p 8000:8000 challengecicd:$BUILD_NUMBER; fi"
                sh "docker rm --force /c1"
                sh "docker run -d --name c1 -p 8000:8000 daafonsecato/challengecicd:$BUILD_NUMBER"
                sh "docker container ls"
			}
		}


	}
    post{
        always { cleanWs() }
    }
}