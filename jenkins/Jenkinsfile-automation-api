String sectionHeaderStyle = '''
    color: black;
	background: #f0f0f0;
	sans-serif !important;
	padding: 5px;
	text-align: center;
	font-weight: bold;
'''

String separatorStyle = '''
    border: 0;
	border-bottom: 0px;
	dashed #ccc;
	background: #999;
'''
properties([
    parameters([
        [
            $class: 'ParameterSeparatorDefinition',
            sectionHeader: 'TEST PARAMETERS',
            separatorStyle: separatorStyle,
            sectionHeaderStyle: sectionHeaderStyle
        ],
       		choice(name: 'TEST_TAG', choices: ['Critical', 'Regression','Specific_case'], description: 'Select the test cases you want to run'),
        [
			$class: 'DynamicReferenceParameter', choiceType: 'ET_FORMATTED_HTML', description: 'Add the test case tag you want to run (e.g. Boards, Lists, Cards).', name: 'TEST_CASE', omitValueField: true, randomName: 'choice-parameter-2932013866939037', referencedParameters: 'TEST_TAG',
			script: [$class: 'GroovyScript', fallbackScript: [classpath: [], sandbox: false, script: 'return null'], 
			script: [classpath: [], sandbox: false, script: '''if(TEST_TAG.equals("Specific_case")){
				return "<input name=\\"value\\" class=\\"setting-input\\" type=\\"text\\">"}
			else{return "n/a<input type=\\"hidden\\" name=\\"value\\" value=\\"n/a\\" />"}''']]
		],
        [
            $class: 'ParameterSeparatorDefinition',
            sectionHeader: 'NOTIFICATION',
            separatorStyle: separatorStyle,
            sectionHeaderStyle: sectionHeaderStyle
        ],
        validatingString(defaultValue: '', description: 'Please add the email where you want to be notified about this process. Allowed domains @endava.com (CASE SENSITIVE)', failedValidationMessage: 'The value entered does not match with an email allowed.', name: 'NOTIFICATION_EMAIL', regex: '^.*@(endava)*(\\.\\w{2,3})+$')   
    ])
])
pipeline {
    agent {label 'master'}
    environment {
		GIT_CREDENTIALS = credentials ('git-credentials')
        SLACK_TOKEN = credentials ('slack-token')
	}
	tools { 
        maven 'maven 3.6.3'
    }
	stages {
		stage ('CODE') {
            steps {
                echo "** Clone automation code **"
                git branch: 'API', credentialsId: "${env.GIT_CREDENTIALS}", url: 'git@gitlab.com:cd-plus-ta-workshop/automation-api.git'
            }
        }
        stage ('TEST') {
            steps {
                echo "** Making tests.. **"
                script {
                    env.BUILD_USER = getBuilduser("${params.NOTIFICATION_EMAIL}")
                    env.TEST = getTest("${params.TEST_TAG}", "${params.TEST_CASE}")
                    sh """mvn test -Dcucumber.options='--tags @${env.TEST}'\
						-Dtest=RunTestBase"""
				}
            }
        }
        stage ('REPORTS'){
			steps{
                publishHTML([
					allowMissing         : true,
					alwaysLinkToLastBuild: true,
					keepAll              : true,
					reportDir            : "target/cucumber-reports",
					reportFiles          : 'index.html',
					reportName           : 'HTML Reports',
					reportTitles         : 'Standard Report'
				])
				junit "target/cucumber-reports/cucumber-results.xml"
				cucumber fileIncludePattern: "target/cucumber-reports/cucumber.json"
				livingDocs featuresDir: "./target/cucumber-reports", format: 'PDF'
			}
		}
	}
    post{
        always { cleanWs() }
        failure {
            slackSend baseUrl: 'https://endavabogota.slack.com/services/hooks/jenkins-ci/', channel: '#jenkins-notifications', color: 'danger', message: "${env.JOB_NAME}: Failed build launch by ${env.BUILD_USER}", token: "${env.SLACK_TOKEN}"
        }
        success {
            slackSend baseUrl: 'https://endavabogota.slack.com/services/hooks/jenkins-ci/', channel: '#jenkins-notifications', color: 'good', message: "${env.JOB_NAME}: Success build launch by ${env.BUILD_USER}", token: "${env.SLACK_TOKEN}"
        }
        unstable {
            slackSend baseUrl: 'https://endavabogota.slack.com/services/hooks/jenkins-ci/', channel: '#jenkins-notifications', color: 'warning', message: "${env.JOB_NAME}: Unstable build launch by ${env.BUILD_USER}", token: "${env.SLACK_TOKEN}"
        }
    }
}
def getBuilduser (String email) {
	return email.split("@")[0]
}
def getTest(String test_tag, String test_case) {
	if (test_tag.contains("Specific_case")){
        return "$test_case"
    } else {
        return "$test_tag"
    }
}