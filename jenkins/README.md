### *Jenkins as CI/CD tool*

Deepening in Jenkins tool with a complete provisioning of concepts, interaction, and an isolated environment with a focus on testing.

#### Subtopics:
* Installation
  - Using configuration management.
  - Jenkins with ansible.
  - A brief view of Jenkins dashboard.
* Credentials
  - Types.
  - Credentials creation.
* Job types
  - Advantages and disadvantages of each one.
  - Job creation.
* Plugins
  - What are?.
  - Useful plugins for testing.
  - Running jobs and report results.
* Running jobs in different nodes
  - Agents and executors.
  - Definition in the pipelines.
  - Master-slave architecture.
  - Launch methods.
  - Take a look at the nodes configuration in Jenkins dashboard.
* Additional helpful tools.

- - -

In this folder, you'll find the vagrant directory with Vagrantfile which sets all the needed configuration for creating a virtual machine included install Jenkins using ansible-playbooks, as well as setting the networking part of the virtual machine and some other specific programs needed to run automation test jobs.

## How to use this folder

### Prerequisites

* Install VirtualBox
  * Here is a [link](https://www.virtualbox.org/wiki/Downloads) for you to download it.
    In case the VirtualBox pages are blocked for you, follow this [link](https://gitlab.com/cd-plus-ta-workshop/cicd/-/tree/master/virtual-box-installers) and download your respective installer. 
  * How to in [Windows](https://youtu.be/v520HJNiETE?t=55), [Linux](https://www.youtube.com/watch?v=OD_mA2riAhU) and [macOS](https://youtu.be/bQVAT5SntKk?t=36) in case you need it. 
    <br/>
     >In the special case that you have a MacOS with Catalina version is possible the installation failed so please go to  `System Preferences - Security & Privacy - Allow apps downloaded from` section. 
     Here you will see the package that was blocked, allow the package and try to install again VirtualBox. 

    ![packageunblocked](../../virtual-box-installers/UnblockPackageCatalinaMacOSversion.jpg)
    <br/>
* Install Vagrant and operative system box.
  * How to in [Windows](https://www.youtube.com/watch?v=d6MBW9MoIDk), [Ubuntu](https://www.youtube.com/watch?v=bQTBBV-3T6k) and [macOS](https://youtu.be/OzJlR2qcTHU?t=37) in case you need it.
    In case you have a Windows as OS, you have to go to `Turn Windows feature on or off` on your pc and disable `Hyper-V` function, before install vagrant. You have to restart your pc to apply this change.
  * Here is the [link](https://www.vagrantup.com/downloads.html) for you to download it.*
  * Open a terminal.
  * Verify if the installation was successfully running the next command:
  `vagrant --version`
  * We have to download a Vagrant box to have the version that we want for our OS, so run on the terminal the next command: 
  `vagrant box add ubuntu/bionic64`

**Note: Please install the latest version (2.2.9)*

Now that our computer has installed and configured Vagrant and VirtualBox, download this repo with a .zip or clone it into your computer.

### Steps to start the Jenkins local environment:

* Make sure that you are on the directory on which you can find the Vagrantfile. Run:
`cd vagrant/`
* Open a terminal on that directory. Please use *Git Bash*.
* Now you are ready to start the machine:
`vagrant up`
This takes time (20 min approx) please leave it to run and wait until it finished and then follow the next steps.
* Verify if the machine was created. Go to `Oracle VM VirtualBox` program, the machine should appear there with "jenkins-local-environment" name.
* After the installation and configuration are all set up, if you want to connect from your computer to your local machine run:
`vagrant ssh`
* Go to [http://localhost:8082/](http://localhost:8082/) in your browser. The username is 'admin' and password too.

Now you have your Jenkins local environment up and ready to explore!

## Test automation setup in Jenkins

1. We have to configurate mvn installation to run the testing suite. 
   * Go to `Manage Jenkins`, select `Global Tool Configuration`, scroll down, click in `Maven installations` and `Add Maven`.
   * Fill `Name` with *'maven 3.6.3'*. This is the id that we will use in Jenkinsfile.
   * Check `Install automatically`.
   * Choose *'3.6.3'* in `Version` choice list.
   * Click in `Save` button.
2. Add '*git-credentials*' credential.
   * GitLab configuration:
     * Go to the inside of the jenkins-local-environment machine.
      Remember you have to run `vagrant ssh` in the path where is the Vagrantfile.
     * Run the next command:
      `sudo cat /var/lib/jenkins/.ssh/id_rsa.pub`
     * In the same box, after you have already copied your key, add an space and in that space, write down your email (the one associated with gitlab) like this: ssh-rsa AAAAB3Nza zv0Bzw== myawesome@email.com.
     * Go to SSH keys setup of your GitLab profile with this [link](https://gitlab.com/profile/keys). 
     * Paste the *'id_rsa.pub file'* content in `Key` box.
     * In the same box writes your email with space as the separation between the key and your email.
     * If you want to add an expiration date for that key, add it in `Expires at` option..  
     * Finally click in `Add key` button
   * Jenkins configuration:
     * Go to inside again of the jenkins-local-environment machine.
     * Run the next command:
      `sudo cat /var/lib/jenkins/.ssh/id_rsa`
     * Copy the output of that command. You have to copy all output since *'-----BEGIN RSA PRIVATE KEY----- ...'* until *'... -----END RSA PRIVATE KEY-----'*.
     * Go to `Manage Jenkins` and select `Manage credentials`.
     * Click in `(global)` word that is `Domain` list.
     * Click in `Add Credentials` that is on the left side.
     * Choose *'SSH Username with private key'* in `Kind` choice list.
     * Fill `ID` box with *'git-credentials'* words. Please use this name because we called it in the Jenkins in this way.
     * Fill `Description` box with any description that you want.
     * Fill `Username` box with your GitLab email.
     * Check `Enter directly` in `Private Key` option. This selection should deploy a `Key` box.
     * Click in `Add` in `Key` box and fill the space with *'id_rsa file'* that we copied in the first steps of this section.
     * Leave empty the `Passphrase` box because we didn't set a password for these keys.
     * Click in `OK` button to save our *'git-credentials'* credential.
3. Add '*slack-token*' credential.
   * Slack configuration:
     * Go to page of the Jenkins integration with your slack using this [link](https://endavabogota.slack.com/apps/A0F7VRFKN-jenkins-ci). 
     * Click in `Add to Slack` button.
     * Choose the channel where you can publish the slack messages. You have to be a member of this channel.
     * Click in `Add Jenkins CI Integration` button.
     * The page launch all steps to link slack with Jenkins. Scroll down until you find a `Token` box in `Integration Settings` and copy this random token.
     * Click `Save Settings` button.
   * Jenkins configuration:
     * Go to `Manage Jenkins` and select `Manage credentials`.
     * Click in `(global)` word that is `Domain` list.
     * Click in `Add Credentials` that is on the left side.
     * Choose *'Secret text'* in `Kind` choice list.
     * Fill `Secret` box with the token that we copy in *'Slack configuration'* section.
     * Fill `ID` box with *'slack-token'* words. Please use this name because we called it in the Jenkins in this way.
     * Fill `Description` box with any description that you want.
     * Click in `OK` button to save our *'slack-token'* credential.
4. Create the automation testing jobs pipeline type (*'automation-ui'* and *'automation-api'*).
   * Go to the principal screen of [Jenkins](http://localhost:8082/).
   * Click in `New item` option that is in the left panel.
   * Enter a job name can be *'automation-ui-test'* or *'automation-api-test'*.
   * Select `Pipeline` type of Jenkins job.
   * Click in `OK` button to create your first job.
   * In order to configure the origin of the pipeline that Jenkins follow in this job go to the `Pipeline` section.
   * Choose *'Pipeline script from SCM'* in `Definition` choice list.
   * Choose *'Git'* in `SCM` choice list.
   * Copy the ssh link *'git@gitlab.com:cd-plus-ta-workshop/cicd.git'* in `Repository URL` box.
   * Select '*git-credentials*' credential `Credentials` in choice list.
   * Change `Script Path` with '*jenkins/Jenkinsfile-automation-api'* or '*jenkins/Jenkinsfile-automation-ui'* depending of that you want to test.
   * Leave the same default settings in the other options.
   * Click in `Save` button.
5. Click in `Build Now` to run the job and test all connections.
6. In the second time that you build the job, you can `Build with Parameters`. If you want to enjoy all the options, you have to approve the parameter script:
   * Go to `Manage Jenkins` and select `In-process Script Approval`.
   * Click in all `Approve` buttons that exist.

When the job has been finished you can click the `blue, yellow or red circle` in `Build history` and this redirects to `Console Output` where you can check all logs that Jenkinsfile created. 
If the build was successful, you can check the results in the report that are found in the left panel like `HTML Reports`, `Test Result`, `Cucumber reports` and `Living Documentation`.

7. Create the manual testing job freestyle type (*'manual-api'*).
   * Go to the principal screen of [Jenkins](http://localhost:8082/).
   * Click in `New item` option that is in the left panel.
   * Enter a job name can be *'manual-api'*.
   * Select `Freestyle project` type of Jenkins job.
   * Click in `OK` button to create the job.
   * In order to configure the origin of the code, go to `Source Code Management` section.
   * Choose *'Git'* in `SCM` choice list.
   * Copy the ssh link *'git@gitlab.com:cd-plus-ta-workshop/manual-api.git'* in `Repository URL` box.
   * Select '*git-credentials*' credential `Credentials` in choice list.
   * In order to configure the test to run with newman, go to `Build` section.
   * Click in `Add build step` button and select `Execute shell` option in the list.
   * Copy the next command in `Command` box that appear with before selection.
    `newman run TrelloAPI.postman_collection.json -e Production.postman_environment.json`
   * Additional click in `Advanced...` button and write *'1'* in `Exit code to set build unstable` box.
   * Click again in `Add build step` button and select `Execute shell` option in the list.
   * Copy the next command in `Command` box that appear with before selection.
    `newman run TrelloAPI.postman_collection.json -e Production.postman_environment.json --reporters htmlextra`
   * Please repeat click in `Advanced...` button and write *'1'* in `Exit code to set build unstable` box.
   * In order to generate visual reports, go to `Post-build Actions` section.
   * Click in `Add post-build action` button and select `Publish HTML reports` option in the list.
   * Fill with *'newman'* word the `HTML directory to archive` box.
   * Fill with '*' the `Index page[s]` box.
   * When we have commands that generate files, is important add a last step. For that make a click again in `Add post-build action` button and select `Delete workspace when build is done` option in the list.
   * Click in `Save` button.
 1. Click in `Build Now` to run the job and test all connections.
  When the job has been finished you can click the `blue, yellow or red circle` in `Build history` and this redirects to `Console Output` where you can check all logs that Jenkinsfile created. 
  Also you can check the results in the newman report that are found in the left panel with `HTML Reports` name, you can click one and appear the *'Newman report'*. 